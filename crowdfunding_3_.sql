-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le : lun. 30 jan. 2023 à 12:32
-- Version du serveur : 5.7.36
-- Version de PHP : 7.4.26

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `crowdfunding`
--

DELIMITER $$
--
-- Procédures
--
DROP PROCEDURE IF EXISTS `nb_contributors`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `nb_contributors` (IN `idProject` INT)  BEGIN
SELECT COUNT(*) FROM (SELECT COUNT(contributions.id_user) FROM contributions WHERE contributions.id_project = idProject GROUP BY contributions.id_user) as countContribution;
END$$

DROP PROCEDURE IF EXISTS `totalContributionById`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `totalContributionById` (IN `idUser` INT)  BEGIN 
SELECT sum(contributions.montant) FROM `contributions` WHERE contributions.id_user=idUser;
END$$

--
-- Fonctions
--
DROP FUNCTION IF EXISTS `pair_impair`$$
CREATE DEFINER=`root`@`localhost` FUNCTION `pair_impair` (`number` INT) RETURNS VARCHAR(20) CHARSET latin1 IF number % 2 <> 0
          THEN 
          set @affichage = CONCAT(number, ' est impair !');
          return @affichage;
          ELSE
          set @affichage = CONCAT(number, ' est pair !');
          return @affichage;
      END IF$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Structure de la table `contributions`
--

DROP TABLE IF EXISTS `contributions`;
CREATE TABLE IF NOT EXISTS `contributions` (
  `id_contribution` int(11) NOT NULL AUTO_INCREMENT,
  `id_user` int(11) DEFAULT NULL,
  `id_project` int(11) DEFAULT NULL,
  `montant` decimal(10,2) DEFAULT NULL,
  `date` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_contribution`),
  KEY `id_project` (`id_project`),
  KEY `id_user` (`id_user`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `contributions`
--

INSERT INTO `contributions` (`id_contribution`, `id_user`, `id_project`, `montant`, `date`) VALUES
(1, 3, 1, '6.00', '2012-06-18 10:34:09'),
(2, 3, 1, '6.00', '2012-06-18 10:34:09'),
(3, 2, 1, '110.00', '2012-06-19 10:11:09'),
(4, 3, 1, '40.00', '2012-07-18 15:19:14'),
(5, 3, 2, '8.40', '2012-06-18 10:38:49'),
(6, 2, 2, '170.00', '2012-06-18 10:54:11'),
(7, 4, 2, '500.00', '2012-06-18 11:11:11'),
(8, 3, 2, '850.00', '2012-06-18 11:16:09'),
(9, 4, 2, '1150.00', '2012-06-18 11:34:11'),
(22, 2, 2, '100.00', '2023-01-01 10:00:10'),
(23, 3, 2, '500.00', '2023-01-01 10:00:10'),
(24, 2, 2, '500.00', '2023-01-01 10:00:10'),
(25, 3, 2, '10.00', '2023-01-01 10:16:10'),
(26, 2, 3, '1800.00', '2023-01-06 15:43:35'),
(27, 2, 3, '260.00', '2023-01-06 15:44:30'),
(28, 4, 4, '15000.00', '2023-01-06 15:53:55');

--
-- Déclencheurs `contributions`
--
DROP TRIGGER IF EXISTS `actualisation_project`;
DELIMITER $$
CREATE TRIGGER `actualisation_project` AFTER INSERT ON `contributions` FOR EACH ROW BEGIN
DECLARE colAmt FLOAT;
DECLARE askAmt FLOAT;
	UPDATE `projects`
    SET projects.collectedAmount = projects.collectedAmount + new.montant,
    projects.nb_contribution = projects.nb_contribution +1
    WHERE id_project = new.id_project;
    
SELECT collectedAmount INTO colAmt FROM projects WHERE id_project = new.id_project;
SELECT askedAmount INTO askAmt FROM projects WHERE id_project = new.id_project;

    IF colAmt >= askAmt THEN 
    	UPDATE `projects`
    	SET projects.is_finished = true
    	WHERE id_project = new.id_project;
	END IF;
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Structure de la table `projects`
--

DROP TABLE IF EXISTS `projects`;
CREATE TABLE IF NOT EXISTS `projects` (
  `id_project` int(11) NOT NULL AUTO_INCREMENT,
  `name_project` varchar(100) NOT NULL,
  `project_categorie` varchar(255) NOT NULL,
  `decription` longtext,
  `askedAmount` float NOT NULL,
  `collectedAmount` float DEFAULT '0',
  `nb_contribution` int(11) NOT NULL DEFAULT '0',
  `is_finished` tinyint(1) NOT NULL DEFAULT '0',
  `id_user` int(11) NOT NULL,
  PRIMARY KEY (`id_project`),
  KEY `id_user` (`id_user`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `projects`
--

INSERT INTO `projects` (`id_project`, `name_project`, `project_categorie`, `decription`, `askedAmount`, `collectedAmount`, `nb_contribution`, `is_finished`, `id_user`) VALUES
(1, 'Combat contre la faim', 'Solidarité', 'Pour aider a lutter contre la faim du monde, l\'appétit-callypse, l\'apéro-guédon, la dévorestation et la carbonaranicule. ', 15000, 2678.4, 4, 0, 1),
(2, 'Donation à la fondation', 'Solidarité', 'Pour le bon fonctionnement de la fondation.Solidarité', 1000, 1266, 9, 1, 1),
(3, 'DC doit vaincre Marvel', 'Cinéma', 'Parce que nous nous devons de valoriser les vrais héros, tu dois rejoindre batman, aquaman, superman, yeah-man, cai-man, talis-man, walk-man et toxico-man.', 1000000, 2060, 2, 0, 1),
(4, 'Start-up dans la gourde new age', 'innovation', 'creation d\'une gourde auto remplissante de divers lisquides en fabrication direct a base d\'air', 20000, 15000, 1, 0, 1);

-- --------------------------------------------------------

--
-- Structure de la table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id_user` int(11) NOT NULL AUTO_INCREMENT,
  `type` enum('personn','company','association','fondation') DEFAULT NULL,
  `status` enum('contributor','creator') DEFAULT NULL,
  `name_user` varchar(100) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `adresse` varchar(255) DEFAULT NULL,
  `city` varchar(100) DEFAULT NULL,
  `zipCode` varchar(50) DEFAULT NULL,
  `country` varchar(100) DEFAULT NULL,
  `IBAN` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id_user`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `users`
--

INSERT INTO `users` (`id_user`, `type`, `status`, `name_user`, `phone`, `email`, `password`, `adresse`, `city`, `zipCode`, `country`, `IBAN`) VALUES
(1, 'fondation', 'creator', 'A vot\' bon coeur', '06 66 66 66 66', 'fondation-du-coeur@mail.com', 'qkj4DhvqF4liufFd', '6 rue de l\'abnégation', 'Lyon', '69000', 'France', 'FR76 6666 6666 6666 6666 6666 666'),
(2, 'personn', 'contributor', 'Axel', '06 55 56 66 66', 'axel@mail.com', 'iuqhdsflidsqfl', '6 rue de lautre coté', 'Lyon', '69000', 'France', NULL),
(3, 'personn', 'contributor', 'Erwin', '06 55 56 96 66', 'erwin@mail.com', 'cestunmdpquitue54836', '9 avenue d encore plus loin', 'Lyon', '69000', 'France', NULL),
(4, 'personn', 'contributor', 'Mathieu', '06 55 56 50 56', 'mathieu@mail.com', 'gFD_63?hjfçU3_k', '86 rue de lapéro', 'peirraud', '26260', 'France', NULL);

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `contributions`
--
ALTER TABLE `contributions`
  ADD CONSTRAINT `contributions_ibfk_1` FOREIGN KEY (`id_project`) REFERENCES `projects` (`id_project`),
  ADD CONSTRAINT `contributions_ibfk_2` FOREIGN KEY (`id_user`) REFERENCES `users` (`id_user`);

--
-- Contraintes pour la table `projects`
--
ALTER TABLE `projects`
  ADD CONSTRAINT `projects_ibfk_1` FOREIGN KEY (`id_user`) REFERENCES `users` (`id_user`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
